import React, {Component} from 'react';
import './networkGraph.scss';
import * as d3 from 'd3';
import PropTypes from 'prop-types';

class Network extends Component {
    state = {
        width: 1024,
        height: 768,
        gridSize: 100,
        selections: {},
        simulation: null,
        zoom: null,
        forceProperties: {
            center: {
                x: 0.5,
                y: 0.5,
            },
            charge: {
                enabled: true,
                strength: -700,
                distanceMin: 1,
                distanceMax: 2000,
            },
            collide: {
                enabled: true,
                strength: 0.7,
                iterations: 1,
                radius: 35,
            },
            forceX: {
                enabled: true,
                strength: 0.05,
                x: 0.5,
            },
            forceY: {
                enabled: true,
                strength: 0.35,
                y: 0.5,
            },
            link: {
                enabled: true,
                distance: 100,
                iterations: 1,
            },
        },
    };
    computed = {
        innerGridSize: () => {
            return this.state.gridSize / 10;
        },
        // These are needed for captions
        linkTypes: () => {
            const linkTypes = [];
            this.computed.links().forEach((link) => {
                if (linkTypes.indexOf(link.type) === -1) linkTypes.push(link.type);
            });
            return linkTypes.sort();
        },
        classes: () => {
            const classes = [];
            this.computed.nodes().forEach((node) => {
                if (classes.indexOf(node.class) === -1) classes.push(node.class);
            });
            return classes.sort();
        },
        nodes: () => {
            return this.props.data && this.props.data.nodes ? this.props.data.nodes : [];
        },
        links: () => {
            return this.props.data && this.props.data.links ? this.props.data.links : [];
        },
    };

    methods = {
        tick: () => {
            // If no this.props.data is passed to the Vue component, do nothing
            if (!this.props.data) {
                return;
            }
            const transform = (d) => {
                return 'translate(' + d.x + ',' + d.y + ')';
            };

            const link = (d) => {
                // Self-link support
                if (d.source.index === d.target.index) {
                    return `M${d.source.x - 1},${d.source.y - 1}A30,30 -10,1,0 ${
                        d.target.x + 1
                    },${d.target.y + 1}`;
                } else {
                    return (
                        'M' +
                        d.source.x +
                        ',' +
                        d.source.y +
                        ' L' +
                        d.target.x +
                        ',' +
                        d.target.y
                    );
                }
            };

            const graph = this.state.selections.graph;
            graph.selectAll('path').attr('d', link);
            graph.selectAll('circle').attr('transform', transform);
            graph.selectAll('text').attr('transform', transform);

            this.methods.updateNodeLinkCount();
        },
        updateData: () => {
            this.state.simulation.nodes(this.computed.nodes());
            this.state.simulation.force('link').links(this.computed.links());

            const simulation = this.state.simulation;
            const graph = this.state.selections.graph;

            // Links should only exit if not needed anymore
            graph.selectAll('path').data(this.computed.links()).exit().remove();

            graph
                .selectAll('path')
                .data(this.computed.links())
                .enter()
                .append('path')
                .attr('class', (d) => 'link ' + d.type.toLowerCase());

            // Nodes should always be redrawn to avoid lines above them
            graph.selectAll('circle').remove();
            graph
                .selectAll('circle')
                .data(this.computed.nodes())
                .enter()
                .append('circle')
                .attr('r', 30)
                .attr('class', (d) => d.class.toLowerCase())
                .call(
                    d3
                        .drag()
                        .on('start', this.methods.nodeDragStarted)
                        .on('drag', this.methods.nodeDragged)
                        .on('end', this.methods.nodeDragEnded)
                )
                .on('mouseover', this.methods.nodeMouseOver)
                .on('mouseout', this.methods.nodeMouseOut)
                .on('click', this.methods.nodeClick);

            graph.selectAll('text').remove();
            graph
                .selectAll('text')
                .data(this.computed.nodes())
                .enter()
                .append('text')
                .attr('x', 0)
                .attr('y', '.31em')
                .attr('text-anchor', 'middle')
                .text((d) => d.name);

            // Add 'marker-end' attribute to each path
            const svg = d3.select(document.querySelector('svg.graph'));
            svg.selectAll('g')
                .selectAll('path.Manages')
                .attr('marker-end', 'url(#end-manage)');
            svg.selectAll('g').selectAll('path.Owns').attr('marker-end', 'url(#end)');
            // Update caption every time data changes
            this.methods.updateCaption();
            simulation.alpha(1).restart();
        },
        updateForces: (simulation) => {
            const {forceProperties, width, height} = this.state;
            simulation
                .force('center')
                .x(width * forceProperties.center.x)
                .y(height * forceProperties.center.y);
            simulation
                .force('charge')
                .strength(
                    forceProperties.charge.strength * forceProperties.charge.enabled
                )
                .distanceMin(forceProperties.charge.distanceMin)
                .distanceMax(forceProperties.charge.distanceMax);
            simulation
                .force('collide')
                .strength(
                    forceProperties.collide.strength * forceProperties.collide.enabled
                )
                .radius(forceProperties.collide.radius)
                .iterations(forceProperties.collide.iterations);
            simulation
                .force('forceX')
                .strength(
                    forceProperties.forceX.strength * forceProperties.forceX.enabled
                )
                .x(width * forceProperties.forceX.x);
            simulation
                .force('forceY')
                .strength(
                    forceProperties.forceY.strength * forceProperties.forceY.enabled
                )
                .y(height * forceProperties.forceY.y);
            simulation
                .force('link')
                .distance(forceProperties.link.distance)
                .iterations(forceProperties.link.iterations);

            // updates ignored until this is run
            // restarts the simulation (important if simulation has already slowed down)
            simulation.alpha(1).restart();
        },
        updateNodeLinkCount: () => {
            let nodeCount = this.computed.nodes().length;
            let linkCount = this.computed.links().length;

            const highlightedNodes = this.state.selections.graph.selectAll(
                'circle.highlight'
            );
            const highlightedLinks = this.state.selections.graph.selectAll(
                'path.highlight'
            );
            if (highlightedNodes.size() > 0 || highlightedLinks.size() > 0) {
                nodeCount = highlightedNodes.size();
                linkCount = highlightedLinks.size();
            }
            this.state.selections.stats.text(
                'Nodes: ' + nodeCount + ' / Edges: ' + linkCount
            );
        },
        updateCaption: () => {
            // WARNING: Some gross math will happen here!
            const lineHeight = 30;
            const lineMiddle = lineHeight / 2;
            const captionXPadding = 28;
            const captionYPadding = 5;

            const caption = this.state.selections.caption;
            caption
                .select('rect')
                .attr(
                    'height',
                    captionYPadding * 2 +
                        lineHeight *
                            (this.computed.classes().length +
                                this.computed.linkTypes().length)
                );

            const linkLine = (d) => {
                const source = {
                    x: captionXPadding + 13,
                    y:
                        captionYPadding +
                        (lineMiddle + 1) +
                        lineHeight * this.computed.linkTypes().indexOf(d),
                };
                const target = {
                    x: captionXPadding - 10,
                };
                return 'M' + source.x + ',' + source.y + 'H' + target.x;
            };

            caption.selectAll('g').remove();
            const linkCaption = caption.append('g');
            linkCaption
                .selectAll('path')
                .data(this.computed.linkTypes())
                .enter()
                .append('path')
                .attr('d', linkLine)
                .attr('class', (d) => 'link ' + d.toLowerCase());

            linkCaption
                .selectAll('text')
                .data(this.computed.linkTypes())
                .enter()
                .append('text')
                .attr('x', captionXPadding + 20)
                .attr(
                    'y',
                    (d) =>
                        captionYPadding +
                        (lineMiddle + 5) +
                        lineHeight * this.computed.linkTypes().indexOf(d)
                )
                .attr('class', 'caption')
                .text((d) => d);

            const classCaption = caption.append('g');
            classCaption
                .selectAll('circle')
                .data(this.computed.classes())
                .enter()
                .append('circle')
                .attr('r', 10)
                .attr('cx', captionXPadding - 2)
                .attr(
                    'cy',
                    (d) =>
                        captionYPadding +
                        lineMiddle +
                        lineHeight *
                            (this.computed.linkTypes().length +
                                this.computed.classes().indexOf(d))
                )
                .attr('class', (d) => d.toLowerCase());

            classCaption
                .selectAll('text')
                .data(this.computed.classes())
                .enter()
                .append('text')
                .attr('x', captionXPadding + 20)
                .attr(
                    'y',
                    (d) =>
                        captionYPadding +
                        (lineMiddle + 5) +
                        lineHeight *
                            (this.computed.linkTypes().length +
                                this.computed.classes().indexOf(d))
                )
                .attr('class', 'caption')
                .text((d) => d);

            const captionWidth = caption.node().getBBox().width;
            const captionHeight = caption.node().getBBox().height;
            const paddingX = 18;
            const paddingY = 12;
            caption.attr(
                'transform',
                'translate(' +
                    (this.state.width - captionWidth - paddingX) +
                    ', ' +
                    (this.state.height - captionHeight - paddingY) +
                    ')'
            );
        },
        zoomed: () => {
            const transform = d3.event.transform;
            // The trick here is to move the grid in a way that the user doesn't perceive
            // that the axis aren't really moving
            // The actual movement is between 0 and gridSize only for x and y
            const translate =
                (transform.x % (this.state.gridSize * transform.k)) +
                ',' +
                (transform.y % (this.state.gridSize * transform.k));
            this.state.selections.grid.attr(
                'transform',
                'translate(' + translate + ') scale(' + transform.k + ')'
            );
            this.state.selections.graph.attr('transform', transform);

            // Define some world boundaries based on the graph total size
            // so we don't scroll indefinitely
            const graphBox = this.state.selections.graph.node().getBBox();
            const margin = 200;
            const worldTopLeft = [graphBox.x - margin, graphBox.y - margin];
            const worldBottomRight = [
                graphBox.x + graphBox.width + margin,
                graphBox.y + graphBox.height + margin,
            ];
            this.state.zoom.translateExtent([worldTopLeft, worldBottomRight]);
        },
        nodeDragStarted: (d) => {
            if (!d3.event.active) {
                this.state.simulation.alphaTarget(0.3).restart();
            }
            d.fx = d.x;
            d.fy = d.y;
        },
        nodeDragged: (d) => {
            d.fx = d3.event.x;
            d.fy = d3.event.y;
        },
        nodeDragEnded: (d) => {
            if (!d3.event.active) {
                this.state.simulation.alphaTarget(0.0001);
            }
            d.fx = null;
            d.fy = null;
        },
        nodeMouseOver: (d) => {
            const graph = this.state.selections.graph;
            const circle = graph.selectAll('circle');
            const path = graph.selectAll('path');
            const text = graph.selectAll('text');

            const related = [];
            const relatedLinks = [];
            related.push(d);
            this.state.simulation
                .force('link')
                .links()
                .forEach((link) => {
                    if (link.source === d || link.target === d) {
                        relatedLinks.push(link);
                        if (related.indexOf(link.source) === -1) {
                            related.push(link.source);
                        }
                        if (related.indexOf(link.target) === -1) {
                            related.push(link.target);
                        }
                    }
                });
            circle.classed('faded', true);
            circle.filter((df) => related.indexOf(df) > -1).classed('highlight', true);
            path.classed('faded', true);
            path.filter((df) => df.source === d || df.target === d).classed(
                'highlight',
                true
            );
            text.classed('faded', true);
            text.filter((df) => related.indexOf(df) > -1).classed('highlight', true);
            // This ensures that tick is called so the node count is updated
            this.state.simulation.alphaTarget(0.0001).restart();
        },
        nodeMouseOut: () => {
            const graph = this.state.selections.graph;
            const circle = graph.selectAll('circle');
            const path = graph.selectAll('path');
            const text = graph.selectAll('text');

            circle.classed('faded', false);
            circle.classed('highlight', false);
            path.classed('faded', false);
            path.classed('highlight', false);
            text.classed('faded', false);
            text.classed('highlight', false);
            // This ensures that tick is called so the node count is updated
            this.state.simulation.restart();
        },
        nodeClick: (d) => {
            if (this.props.onNodeClick && typeof this.props.onNodeClick === 'function') {
                this.props.onNodeClick(d);
            }
            const circle = this.state.selections.graph.selectAll('circle');
            circle.classed('selected', false);
            circle.filter((td) => td === d).classed('selected', true);
            this.state.selections.graph
                .selectAll('circle')
                .data({name: 'new one', group: 1, class: 'Brand'})
                .enter()
                .append('circle')
                .attr('r', 30)
                .attr('class', (d) => d.class.toLowerCase())
                .call(
                    d3
                        .drag()
                        .on('start', this.methods.nodeDragStarted)
                        .on('drag', this.methods.nodeDragged)
                        .on('end', this.methods.nodeDragEnded)
                )
                .on('mouseover', this.methods.nodeMouseOver)
                .on('mouseout', this.methods.nodeMouseOut)
                .on('click', this.methods.nodeClick);
            this.state.simulation.alpha(1).restart();
        },
    };

    componentDidMount() {
        // You can set the component width and height in any way
        // you prefer. It's responsive! :)
        const simulation = d3
            .forceSimulation()
            .force(
                'link',
                d3.forceLink().id((d) => d.id)
            )
            .force('charge', d3.forceManyBody())
            .force('collide', d3.forceCollide())
            .force('center', d3.forceCenter())
            .force('forceX', d3.forceX())
            .force('forceY', d3.forceY());
        this.setState({
            width: Math.floor((window.innerWidth * 9) / 10),
            height: Math.floor((window.innerHeight * 6) / 10),
            simulation: simulation.on('tick', this.methods.tick),
        });
        // Call first time to setup default values
        this.methods.updateForces(simulation);

        const svg = d3.select(document.querySelector('svg.graph'));
        // Define the arrow marker
        svg.append('svg:defs')
            .selectAll('marker')
            .data(['end']) // Different link/path types can be defined here
            .enter()
            .append('svg:marker') // This section adds in the arrows
            .attr('id', String)
            .attr('viewBox', '0 -5 10 10')
            .attr('refX', 43) // Prevents arrowhead from being covered by circle
            .attr('refY', 0)
            .attr('markerWidth', 6)
            .attr('markerHeight', 6)
            .attr('orient', 'auto')
            .attr('class', 'Owns')
            .append('svg:path')
            .attr('d', 'M0,-5L10,0L0,5');

        svg.append('svg:defs')
            .selectAll('marker')
            .data(['end-manage']) // Different link/path types can be defined here
            .enter()
            .append('svg:marker') // This section adds in the arrows
            .attr('id', String)
            .attr('viewBox', '0 -5 10 10')
            .attr('refX', 43) // Prevents arrowhead from being covered by circle
            .attr('refY', 0)
            .attr('markerWidth', 6)
            .attr('markerHeight', 6)
            .attr('class', 'Manages')
            .attr('orient', 'auto')
            .append('svg:path')
            .attr('d', 'M0,-5L10,0L0,5');
        // Define arrow for self-links
        svg.append('svg:defs')
            .selectAll('marker')
            .data(['end-self'])
            .enter()
            .append('svg:marker') // This section adds in the arrows
            .attr('id', String)
            .attr('viewBox', '0 -5 10 10')
            .attr('refX', 40)
            .attr('refY', -15)
            .attr('markerWidth', 6)
            .attr('markerHeight', 6)
            .attr('orient', 285)
            .append('svg:path')
            .attr('d', 'M0,-5L10,0L0,5');

        // Add zoom and panning triggers
        let zoom = d3
            .zoom()
            .scaleExtent([1 / 4, 4])
            .on('zoom', this.methods.zoomed);
        this.setState({
            zoom: zoom,
        });
        svg.call(zoom);

        // A background grid to help user experience
        // The width and height depends on the minimum scale extent and
        // the + 10% and negative index to create an infinite grid feel
        // The precedence of this element is important since you'll have
        // click events on the elements above the grid
        const grid = svg
            .append('rect')
            .attr('x', '-10%')
            .attr('y', '-10%')
            .attr('width', '410%')
            .attr('height', '410%')
            .attr('fill', 'url(#grid)');
        const graph = svg.append('g');
        const stats = svg
            .append('text')
            .attr('x', '1%')
            .attr('y', '98%')
            .attr('text-anchor', 'left');
        const caption = svg.append('g');
        this.setState({
            selections: {
                grid: grid,
                graph: graph,
                stats: stats,
                caption: caption,
                svg: d3.select(document.querySelector('svg.graph')),
            },
        });
        caption
            .append('rect')
            .attr('width', '200')
            .attr('height', '0')
            .attr('rx', '10')
            .attr('ry', '10')
            .attr('class', 'caption');
        // setTimeout(() => {
        //     this.methods.updateData();
        // }, 0);
    }

    render() {
        return (
            <div
                style={{
                    width: this.state.width + 'px',
                    height: this.state.height + 'px',
                    margin: 'auto',
                    border: '1px solid black',
                }}
            >
                <svg className="graph" width="100%" height="100%">
                    <defs>
                        <pattern
                            id="innerGrid"
                            width={this.computed.innerGridSize() + '%'}
                            height={this.computed.innerGridSize() + '%'}
                            patternUnits="userSpaceOnUse"
                        >
                            <rect
                                width="100%"
                                height="100%"
                                fill="none"
                                stroke="#CCCCCC7A"
                                strokeWidth="0.5"
                            />
                        </pattern>
                        <pattern
                            id="grid"
                            width={this.state.gridSize + '%'}
                            height={this.state.gridSize + '%'}
                            patternUnits="userSpaceOnUse"
                        >
                            <rect
                                width="100%"
                                height="100%"
                                fill="url(#innerGrid)"
                                stroke="#CCCCCC7A"
                                strokeWidth="1.5"
                            />
                        </pattern>
                    </defs>
                </svg>
            </div>
        );
    }
    componentDidUpdate() {
        this.methods.updateData();
    }
}

Network.propTypes = {
    data: PropTypes.shape({
        nodes: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.string,
                name: PropTypes.string,
                class: PropTypes.string,
            })
        ),
        links: PropTypes.arrayOf(
            PropTypes.shape({
                source: PropTypes.any,
                target: PropTypes.any,
                type: PropTypes.string,
            })
        ),
    }),
    onNodeClick: PropTypes.func,
};

export default Network;
