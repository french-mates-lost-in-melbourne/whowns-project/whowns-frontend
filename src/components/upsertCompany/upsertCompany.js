import React, {Component} from 'react';
import {
    TextField,
    Container,
    Button,
    CircularProgress,
    Backdrop,
    Avatar,
} from '@material-ui/core';
import {connect} from 'react-redux';
import {withBase} from '../baseComponent/baseComponent';
import ActionFactory from '../../actions/actionFactory';
import {ActionType} from '../../actions/actionType';
import './upsertCompany.scss';

class UpsertCompany extends Component {
    state = {
        name: '',
        description: '',
        logo: '',
    };

    width = 128;
    height = 128;

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value,
        });
    };
    uploadFile = (e) => {
        console.log('upload', e.target.files[0]);
        this.toBase64(e.target.files[0])
            .then((value) => this.resize(value))
            .then((base64) => {
                console.log('final base 64', base64);
                this.setState({
                    logo: base64,
                });
            })
            .catch((err) => {
                console.error(err);
            });
    };

    resize = (base64) =>
        new Promise((resolve) => {
            const img = new Image();
            img.src = base64;
            img.onload = () => {
                if (img.width > this.width || img.height > this.height) {
                    const elem = document.createElement('canvas');
                    elem.width = this.width;
                    elem.height = this.height;
                    const ctx = elem.getContext('2d');
                    // img.width and img.height will contain the original dimensions
                    ctx.drawImage(img, 0, 0, this.width, this.height);
                    const resizedBase64 = ctx.canvas.toDataURL('image/png', 1);
                    resolve(resizedBase64);
                } else {
                    resolve(base64);
                }
            };
        });

    toBase64 = (file) =>
        new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = (error) => reject(error);
        });

    submitForm = (e) => {
        console.log(this.props);
        e.preventDefault();
        this.setState({
            formSubmitted: true,
        });
        if (this.state.name === '' || this.state.description === '') {
            return;
        }
        this.props
            .addCompany({
                name: this.state.name,
                description: this.state.description,
                logo: this.state.logo,
            })
            .then(() => {
                this.setState({
                    formSubmitted: false,
                    name: '',
                    description: '',
                });
                this.props.openSnackBarMessageSuccess('Company added with success');
            })
            .catch((error) => {
                this.setState({
                    formSubmitted: false,
                });
                this.props.openSnackBarMessageError(error.message);
            });
    };

    render() {
        return (
            <div className="upsert-company center background-image">
                <Container className="full-height grid-align-center">
                    <Backdrop className="backdrop" open={this.props.loading}>
                        <CircularProgress color="inherit" />
                    </Backdrop>
                    <form
                        noValidate
                        autoComplete="off"
                        className="full-height grid-align-center"
                        onSubmit={this.submitForm}
                    >
                        <TextField
                            error={this.state.formSubmitted && this.state.name === ''}
                            label="Name"
                            id="name"
                            value={this.state.name}
                            helperText={
                                this.state.formSubmitted && this.state.name === ''
                                    ? 'Name required.'
                                    : ''
                            }
                            variant="outlined"
                            onChange={this.handleChange}
                            className="white-background input-form"
                        />
                        <input
                            id="logo"
                            accept="image/*"
                            name="logo"
                            className="input-logo"
                            type="file"
                            onChange={this.uploadFile}
                        />
                        <label htmlFor="logo" className="input-form standard-margin-top">
                            <Avatar src={this.state.logo}>L</Avatar>
                            <span>Choose a logo</span>
                        </label>
                        <TextField
                            multiline
                            rows="10"
                            error={
                                this.state.formSubmitted && this.state.description === ''
                            }
                            label="Description"
                            id="description"
                            value={this.state.description}
                            helperText={
                                this.state.formSubmitted && this.state.description === ''
                                    ? 'Description required.'
                                    : ''
                            }
                            variant="outlined"
                            onChange={this.handleChange}
                            className="white-background standard-margin-top input-form"
                        />
                        <Button
                            variant="contained"
                            color="primary"
                            type="submit"
                            className="standard-margin-top"
                        >
                            Create
                        </Button>
                    </form>
                </Container>
            </div>
        );
    }
}

UpsertCompany.propTypes = {
    ...ActionFactory.getActionPropTypes(ActionType.ADD_COMPANY),
};

const mapToProps = (state) => ({
    loading: state.loading.isLoading,
});

const mapDispatch = (dispatch) =>
    ActionFactory.getActions(dispatch, ActionType.ADD_COMPANY);

export default withBase(connect(mapToProps, mapDispatch)(UpsertCompany));
