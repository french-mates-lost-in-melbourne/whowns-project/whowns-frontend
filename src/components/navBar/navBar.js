import React from 'react';
import {NavLink} from 'react-router-dom';
import './navBar.scss';
import logo from '../../assets/images/logo.svg';

const NavBar = () => {
    return (
        <header className="header">
            <NavLink exact to="/">
                <img className="logo" src={logo} alt="logo" />
            </NavLink>
            <div className="empty"></div>
            <nav className="navbar">
                <NavLink className="link" exact to="/">
                    Search
                </NavLink>

                <NavLink className="link" to="/edit">
                    Edit
                </NavLink>
            </nav>
        </header>
    );
};

export default NavBar;
