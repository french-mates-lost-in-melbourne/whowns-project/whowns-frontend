import React, {Component} from 'react';
import {Snackbar} from '@material-ui/core';
import {Alert} from '@material-ui/lab';
import {withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';

export function withBase(WrappedComponent) {
    class BaseHOC extends Component {
        constructor(props) {
            super(props);

            this.state = {
                openSnackBarMessage: false,
                successMessage: '',
                openSnackBarMessageError: false,
                errorMessage: '',
            };
        }

        openSnackBarMessageSuccess(message) {
            this.setState({
                openSnackBarMessage: true,
                successMessage: message,
            });
        }

        openSnackBarMessageError(errorMessage) {
            this.setState({
                openSnackBarMessageError: true,
                errorMessage: errorMessage,
            });
        }

        handleCloseSnackBar = (event, reason) => {
            if (reason === 'clickaway') {
                return;
            }
            this.setState({
                openSnackBarMessage: false,
                openSnackBarMessageError: false,
            });
        };

        render() {
            return (
                <div>
                    <WrappedComponent
                        openSnackBarMessageError={(message) => {
                            this.openSnackBarMessageError(message);
                        }}
                        openSnackBarMessageSuccess={(message) => {
                            this.openSnackBarMessageSuccess(message);
                        }}
                        history={this.props.history}
                    />
                    <Snackbar
                        open={this.state.openSnackBarMessage}
                        autoHideDuration={3000}
                        onClose={this.handleCloseSnackBar}
                    >
                        <Alert onClose={this.handleCloseSnackBar} severity="success">
                            {this.state.successMessage}
                        </Alert>
                    </Snackbar>
                    <Snackbar
                        open={this.state.openSnackBarMessageError}
                        autoHideDuration={3000}
                        onClose={this.handleCloseSnackBar}
                    >
                        <Alert onClose={this.handleCloseSnackBar} severity="error">
                            {this.state.errorMessage}
                        </Alert>
                    </Snackbar>
                </div>
            );
        }
    }

    BaseHOC.propTypes = {
        history: PropTypes.object,
    };

    return withRouter(BaseHOC);
}
