import React, {Component} from 'react';
import {Container, TextField, Backdrop, CircularProgress} from '@material-ui/core';
import {Autocomplete} from '@material-ui/lab';
import {connect} from 'react-redux';
import {withBase} from '../baseComponent/baseComponent';
import PropTypes from 'prop-types';
import ActionFactory from '../../actions/actionFactory';
import {ActionType} from '../../actions/actionType';
import './search.scss';

class Search extends Component {
    openTheDetails = (e, value) => {
        if (value != null) {
            this.props.history.push(`/companies/${value.id}`);
        }
    };

    componentDidMount() {
        console.log(this.state);
        this.props.getCompanies().catch((error) => {
            this.props.openSnackBarMessageError(error.message);
        });
    }

    render() {
        return (
            <div className="search-component full-height background-image">
                <Backdrop className="backdrop" open={this.props.loading}>
                    <CircularProgress color="inherit" />
                </Backdrop>
                <Container>
                    <Autocomplete
                        id="combo-box-demo"
                        options={this.props.companies}
                        getOptionLabel={(option) => option.name}
                        renderInput={(params) => (
                            <TextField
                                {...params}
                                label="Company Name"
                                variant="outlined"
                            />
                        )}
                        className="autocomplete"
                        onChange={this.openTheDetails}
                    />
                </Container>
            </div>
        );
    }
}

Search.propTypes = {
    companies: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string,
        })
    ),
    history: PropTypes.object,
    ...ActionFactory.getActionPropTypes(ActionType.GET_COMPANIES),
};

const mapStateToProps = (state) => {
    return {
        companies: state.companies ?? [],
        loading: state.loading.isLoading,
    };
};

const mapDispatch = (dispatch) =>
    ActionFactory.getActions(dispatch, ActionType.GET_COMPANIES);

export default withBase(connect(mapStateToProps, mapDispatch)(Search));
