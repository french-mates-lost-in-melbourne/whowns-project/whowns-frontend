import {ActionType} from '../actions/actionType';
const init = {
    isLoading: false,
    numberOfLoad: 0,
};

const loadingReducer = (state = init, action) => {
    let numberOfLoad = state.numberOfLoad;
    switch (action.type) {
        case ActionType.ADD_LOADING:
            numberOfLoad++;
            break;
        case ActionType.REMOVE_LOADING:
            numberOfLoad--;
            break;
        default:
    }
    if (numberOfLoad < 0) {
        numberOfLoad = 0;
    }
    return {
        numberOfLoad,
        isLoading: numberOfLoad > 0,
    };
};

export default loadingReducer;
