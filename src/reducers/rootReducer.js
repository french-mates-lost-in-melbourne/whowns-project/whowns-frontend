import {combineReducers} from 'redux';
import companies from './companiesReducer';
import loading from './loadingReducer';

const rootReducer = combineReducers({
    companies,
    loading,
});

export default rootReducer;
