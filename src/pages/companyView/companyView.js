import React, {Component} from 'react';
import {connect} from 'react-redux';
import ActionFactory from '../../actions/actionFactory';
import {ActionType} from '../../actions/actionType';
import PropTypes from 'prop-types';
import './companyView.scss';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Avatar from '@material-ui/core/Avatar';
import {AppBar, Tabs, Tab, Typography, Box} from '@material-ui/core';
import Network from '../../components/networkGraph/networkGraph';

function TabPanel(props) {
    const {children, value, index, ...other} = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && <Box p={3}>{children}</Box>}
        </Typography>
    );
}
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

class CompanyView extends Component {
    state = {
        loading: true,
        tabsValue: 0,
        data: {
            nodes: [
                {id: '1', name: 'Elon Musk', class: 'Worker'},
                {id: '4', name: 'Space x', class: 'Brand'},
                {id: '5', name: 'Tesla', class: 'Brand'},
                {id: '6', name: 'Alphabet', class: 'Brand'},
                {id: '7', name: 'Google', class: 'Brand'},
                {id: '8', name: 'Flutter', class: 'Brand'},
            ],
            links: [
                {source: '1', target: '4', type: 'Manages'},
                {source: '1', target: '5', type: 'Manages'},
                {source: '6', target: '7', type: 'Owns'},
                {source: '6', target: '8', type: 'Owns'},
            ],
        },
    };

    a11yProps(index) {
        return {
            id: `simple-tab-${index}`,
            'aria-controls': `simple-tabpanel-${index}`,
        };
    }
    handleChange = (event, newValue) => {
        this.setState({
            tabsValue: newValue,
        });
    };
    handleNodeClick = (d) => {
        console.log('node clicked', d);
    };
    companyShow = () => {
        let company = this.props.company ? this.props.company : {name: 'No company'};
        console.log('data', this.state.data);

        return (
            <div className="company">
                <div className="empty"></div>
                <Avatar className="company-logo" src={company.logo} alt="company logo">
                    {company.name[0].toUpperCase()}
                </Avatar>
                <p className="company-name">{company.name}</p>
                <div className="company-content">
                    <AppBar position="static">
                        <Tabs
                            value={this.state.tabsValue}
                            onChange={this.handleChange}
                            aria-label="simple tabs example"
                        >
                            <Tab label="Description" {...this.a11yProps(0)} />
                            <Tab label="Links" {...this.a11yProps(1)} />
                        </Tabs>
                    </AppBar>
                    <TabPanel value={this.state.tabsValue} index={0}>
                        <p className="company-desc">{company.description}</p>
                    </TabPanel>
                    <TabPanel value={this.state.tabsValue} index={1}>
                        <Network
                            data={this.state.data}
                            onNodeClick={this.handleNodeClick}
                        />
                    </TabPanel>
                </div>
            </div>
        );
    };

    componentDidMount() {
        // only call if no company found
        if (!this.props.company) {
            this.props.getCompany(this.props.match.params.id);
        }
    }

    render() {
        return (
            <div className="company-view">
                <Backdrop className="backdrop" open={this.props.loading}>
                    <CircularProgress color="inherit" />
                </Backdrop>
                {this.companyShow()}
            </div>
        );
    }
}

const actionUsed = [ActionType.ADD_COMPANY, ActionType.GET_COMPANY];

CompanyView.propTypes = {
    company: PropTypes.shape({
        name: PropTypes.string,
    }),
    ...ActionFactory.getActionPropTypes(...actionUsed),
};

const mapToProps = (state, props) => ({
    company: state.companies
        ? state.companies.find((c) => c && c.id === parseInt(props.match.params.id))
        : null,
    loading: state.loading.isLoading,
});
const mapDispatch = (dispatch) => ActionFactory.getActions(dispatch, ...actionUsed);

export default connect(mapToProps, mapDispatch)(CompanyView);
