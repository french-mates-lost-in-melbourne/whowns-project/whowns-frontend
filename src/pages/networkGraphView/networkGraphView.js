import React, {Component} from 'react';
import Network from '../../components/networkGraph/networkGraph';

class NetworkGraphView extends Component {
    state = {
        data: {
            nodes: [
                {id: '1', name: 'Elon Musk', class: 'Worker'},
                {id: '4', name: 'Space x', class: 'Brand'},
                {id: '5', name: 'Tesla', class: 'Brand'},
                {id: '6', name: 'Alphabet', class: 'Brand'},
                {id: '7', name: 'Google', class: 'Brand'},
                {id: '8', name: 'Flutter', class: 'Brand'},
            ],
            links: [
                {source: '1', target: '4', type: 'Manages'},
                {source: '1', target: '5', type: 'Manages'},
                {source: '6', target: '7', type: 'Owns'},
                {source: '6', target: '8', type: 'Owns'},
            ],
        },
        id: 10,
    };
    handleNodeClick = (d) => {
        console.log('id', typeof d.id);
        console.log(typeof String(1));

        console.log(this.state.data.links[0]);

        this.setState({
            data: {
                nodes: [
                    ...this.state.data.nodes,
                    {
                        id: String(this.state.id),
                        name: 'brand-' + this.state.id,
                        class: 'Brand',
                    },
                ],
                links: [
                    ...this.state.data.links,
                    {source: d.id, target: String(this.state.id), type: 'Manages'},
                ],
            },
            id: this.state.id + 1,
        });
    };

    render() {
        return (
            <div className="network-graph-view">
                <h1>Graph Mock</h1>
                <Network data={this.state.data} onNodeClick={this.handleNodeClick} />
            </div>
        );
    }
}

export default NetworkGraphView;
