import React from 'react';
import './mainPage.scss';
import NetworkGraphView from '../networkGraphView/networkGraphView';
import Search from '../../components/search/search';
import {BrowserRouter, Route} from 'react-router-dom';
import NavBar from '../../components/navBar/navBar';
import UpsertCompany from '../../components/upsertCompany/upsertCompany';
import CompanyView from '../companyView/companyView';

function MainPage() {
    return (
        <BrowserRouter>
            <div className="mainPage">
                <NavBar />
                <div className="content">
                    <Route exact path="/" component={Search} />
                    <Route exact path="/edit" component={UpsertCompany} />
                    <Route exact path="/companies/:id" component={CompanyView} />
                    <Route path="/graph" component={NetworkGraphView} />
                </div>
            </div>
        </BrowserRouter>
    );
}

export default MainPage;
