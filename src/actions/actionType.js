export class ActionType {
    static ADD_COMPANY = 'ADD_COMPANY';
    static GET_COMPANIES = 'GET_COMPANIES';
    static GET_COMPANY = 'GET_COMPANY';
    static SET_COMPANIES = 'SET_COMPANIES';
    static ADD_LOADING = 'ADD_LOADING';
    static REMOVE_LOADING = 'REMOVE_LOADING';

    static getMethodName(actionType) {
        switch (actionType) {
            case this.ADD_COMPANY:
                return 'addCompany';
            case this.GET_COMPANIES:
                return 'getCompanies';
            case this.GET_COMPANY:
                return 'getCompany';
            case this.SET_COMPANIES:
                return 'setCompanies';
            case this.ADD_LOADING:
                return 'addLoading';
            case this.REMOVE_LOADING:
                return 'removeLoading';
            default:
                throw new Error('action type not implemented');
        }
    }
}
