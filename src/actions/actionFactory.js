import {ActionType} from './actionType';
import PropTypes from 'prop-types';
import Axios from 'axios';

export default class ActionFactory {
    static getActions(dispatch, ...actionType) {
        let actionReturn = {};

        actionType.forEach((action) => {
            switch (action) {
                case ActionType.ADD_COMPANY:
                    actionReturn[ActionType.getMethodName(action)] = (company) => {
                        dispatch({type: ActionType.ADD_LOADING});
                        return Axios.post('http://localhost:3030/brands', company)
                            .then((value) => {
                                dispatch({
                                    type: ActionType.ADD_COMPANY,
                                    createdCompany: value.data,
                                });
                                dispatch({type: ActionType.REMOVE_LOADING});
                            })
                            .catch((error) => {
                                dispatch({type: ActionType.REMOVE_LOADING});
                                throw new Error(error);
                            });
                    };
                    break;
                case ActionType.GET_COMPANIES:
                    actionReturn[ActionType.getMethodName(action)] = () => {
                        dispatch({type: ActionType.ADD_LOADING});
                        return Axios.get('http://localhost:3030/brands')
                            .then((value) => {
                                dispatch({
                                    type: ActionType.SET_COMPANIES,
                                    companies: value.data,
                                });
                                dispatch({type: ActionType.REMOVE_LOADING});
                            })
                            .catch((error) => {
                                dispatch({type: ActionType.REMOVE_LOADING});
                                throw new Error(error);
                            });
                    };
                    break;
                case ActionType.GET_COMPANY:
                    actionReturn[ActionType.getMethodName(action)] = (id) => {
                        dispatch({type: ActionType.ADD_LOADING});
                        return Axios.get(`http://localhost:3030/brands/${id}`).then(
                            (value) => {
                                dispatch({
                                    type: ActionType.ADD_COMPANY,
                                    company: value.data,
                                });
                                dispatch({type: ActionType.REMOVE_LOADING});
                            }
                        );
                    };
                    break;
                default:
                    throw new Error('No action for this action type');
            }
        });
        return actionReturn;
    }

    static getActionPropTypes(...actionType) {
        let actionReturn = {};
        actionType.forEach((action) => {
            actionReturn[ActionType.getMethodName(action)] = PropTypes.func;
        });
        return actionReturn;
    }
}
